﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackState : MonoBehaviour {


    // 핵 체력
    public float BossPase1HPHack;
    public float BossPase2HpHack;

    public BossState bossstate;
    float bulletPower;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            bulletPower = col.gameObject.GetComponent<Bullet>().power;
            col.gameObject.GetComponent<Bullet>().DisplayDamage(this.gameObject);
            Destroy(col.gameObject);

            if (bossstate.pase2_on == false)
            {
                BossPase1HPHack -= bulletPower;
                if (BossPase1HPHack <= 0)
                {
                    BossPase1HPHack = 0;
                    bossstate.pase2_on = true;
                    bossstate.isStun = false;
                    bossstate.nowHP = bossstate.BossPase2HPBody;
                    gameObject.SetActive(false); // 핵 HP 끝나면 끔

                }
            }
            else
            {
                BossPase2HpHack -= bulletPower;
                if (BossPase2HpHack <= 0)
                {
                    BossPase2HpHack = 0;
                    gameObject.SetActive(false); // 핵 HP 끝나면 끔
                    bossstate.isDead = true;
                }
            }

        }
    }
}
