﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingSpawn : MonoBehaviour {

    public GameObject Item;
    public float itemSpawnTime;
    public float Timer;

    public bool isItemOn = true;

    public float childCount;

    void Start () {
        Timer = itemSpawnTime;

    }
	
	void Update () {

        childCount = gameObject.transform.childCount;

        if (Timer > 0F && !isItemOn) // 아이템이 생성되지않았다.
        {
            Timer -= Time.deltaTime;
        }
        else if (Timer <=0f && !isItemOn && childCount < 1f) // 타이머는 끝났는데 아이템이 없다.
        {
            GameObject newItem = Instantiate(Item); //회복 아이템 생성
            newItem.transform.position = gameObject.transform.position;
            newItem.transform.parent = gameObject.transform;
            isItemOn = true;
        }

        if (childCount <= 0f) // 회복아이템이 없다.
        {
            isItemOn = false;
        }

        if (childCount > 0f && !isItemOn) // 회복아이템이 있다.
        {
            Timer = itemSpawnTime;
            isItemOn = true;
        }
    }
}
