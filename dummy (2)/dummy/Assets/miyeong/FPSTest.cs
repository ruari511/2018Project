﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSTest : MonoBehaviour
{



    // 회전범위 제한

    public float xSensitivity = 1.0f;
    public float ySensitivity = 1.0f;

    public Camera cam;

    private Vector3 Gap;               // 회전 축적 값.

    public float ro_max;
    public float ro_min;

    public float aaa;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float yRot = Input.GetAxis("Mouse X") * xSensitivity;

        // 값을 축적.
        Gap.x += Input.GetAxis("Mouse Y") * ySensitivity * -1;
        // 카메라 회전범위 제한.
        Gap.x = Mathf.Clamp(Gap.x, ro_max, ro_min);

        cam.transform.localRotation = Quaternion.Euler(Gap.x, 0, 0);//부호 주의
        this.transform.localRotation *= Quaternion.Euler(0, yRot, 0);


    }
}