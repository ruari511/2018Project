﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
    public float hpStart; // 캐릭터 시작 체력
    public float hpMax; // 캐릭터 최대 체력
    public float hpHealSec; // 초당 캐릭터 체력 회복력
    public float def; // 캐릭터 방어력
    public float spd; // 캐릭터 스피드
    public float hpHealingMax; // 캐릭터 자동회복 체력

    public float rotateSpeed;

    Rigidbody rig;

    Vector3 movement;
    float horizontalMove;
    float verticalMove;

    Animator ani;

    public bool DontMove = false;

    public GameManager gm;

    public Text hp_Text = null;

    public FPSTest playertest; // 죽으면 캐릭터의 회전을 멈춤 (화면회전은함)

	public Gun PlayerGun;//현재 가지고 있는 총

    void Awake()
    {
        rig = GetComponent<Rigidbody>();
        ani = gameObject.GetComponent<Animator>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

        hpStart = hpMax;
        StartCoroutine(HPHeal());
    }

    IEnumerator HPHeal() // 1초마다 hp회복
    {
        if (!DontMove)
        {
            if (hpStart < hpHealingMax)
            {
                if (hpStart != 0)
                {
                    float nexthp = hpStart + hpHealSec;

                    if (nexthp >= hpHealingMax)
                    {
                        hpStart = hpHealingMax;
                    }
                    else
                    {
                        hpStart += hpHealSec;
                    }

                }
                if (hpStart > hpMax)
                {
                    hpStart = hpMax;
                }
            }
            yield return new WaitForSeconds(1);
            StartCoroutine(HPHeal());
        }
    }


    void Update()
    { // 키 입력은 여기서

        horizontalMove = Input.GetAxisRaw("Horizontal");
        verticalMove = Input.GetAxisRaw("Vertical");

       
        if (horizontalMove == 0 && verticalMove == 0)
        {
            ani.SetBool("isMoving", false);
        }
        else
        {
            ani.SetBool("isMoving", true);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //gm.ChangeCamera();
        }

    

		//총관련 애니메이션
		if (Input.GetMouseButtonUp(0) && PlayerGun.GunMinload > 0)
			ani.SetTrigger("Shoot");

		if (Input.GetKeyDown(KeyCode.LeftControl))
			ani.SetTrigger("ReLoad");

		hp_Text.text = "HP : " + hpStart;

	}

    void FixedUpdate()
    {
        if (!DontMove)
        {
            Run();
            Die();
        }
        else
        {
            playertest.enabled = false;

        }

    }

    //----------------------------------------------------------

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Enemy")
        {
            hpStart -= other.gameObject.GetComponent<EnemyState>().damage;

            Destroy(other.gameObject);
        }
    }

    //----------------------------------------------------------

    void Run()
    {

       // movement.Set(horizontalMove, 0, verticalMove);
        //movement = movement.normalized * spd * Time.deltaTime;

        //rig.MovePosition(transform.position + movement);


        horizontalMove = horizontalMove * spd * Time.deltaTime;
        verticalMove = verticalMove * spd * Time.deltaTime;

        transform.Translate(Vector3.right * horizontalMove);
        transform.Translate(Vector3.forward * verticalMove);

        //Turn();

    }

    void Turn()
    {
        if (horizontalMove == 0 && verticalMove == 0)
        {
            return;
        }

        Quaternion newRotation = Quaternion.LookRotation(movement);

        rig.rotation = Quaternion.Slerp(rig.rotation, newRotation, rotateSpeed * Time.deltaTime);


    }

    void Die()
    {
        if(hpStart <= 0)
        {
            hpStart = 0;
            ani.SetBool("Die", true);
            DontMove = true;
        }
    }
}