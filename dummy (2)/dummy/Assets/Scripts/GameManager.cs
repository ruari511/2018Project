﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public int camera_N = 0;

    public enum Camera_Number { Start = 0, Shot = 11 };
    void Awake()
    {

        camera_N = 0;

    }

    void Update()
    {

    }

    public void ChangeCamera()
    {

        if (camera_N == (int)Camera_Number.Start)
        {// 현재 상태가 start면 shot으로 바꿈

            camera_N = (int)Camera_Number.Shot;
        }
        else if (camera_N == (int)Camera_Number.Shot)
        {
            camera_N = (int)Camera_Number.Start;

        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("dummy2");
    }
}
