﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal_Item : MonoBehaviour {

    public float heal_amount;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            float player_HP = other.gameObject.GetComponent<PlayerMove>().hpStart;
            float player_HPMax = other.gameObject.GetComponent<PlayerMove>().hpMax;

            if (player_HP < player_HPMax)
            {
                player_HP += heal_amount;

                if (player_HP > player_HPMax)
                {
                    player_HP = player_HPMax;

                }
                other.gameObject.GetComponent<PlayerMove>().hpStart = player_HP;

                Destroy(gameObject);

            }

        }


    }
}
