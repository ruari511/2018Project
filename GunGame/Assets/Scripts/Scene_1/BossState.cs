﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossState : MonoBehaviour {

    public enum Boss_CurrentState { Idle, Chase, Attack, Stun, Death }
    public Boss_CurrentState curState = Boss_CurrentState.Idle;

    public bool isDead; // 보스는 죽지않아요!
    public float ViewFieldRange; // 추적범위 (대상을 추적 할 수있는 최대 거리)
    public float TrackingSpeed; // 추적속도 (추적 상태 시 몬스터의 이동 속도)
    public float TrackingTransform; // 추적전환 (공격 상태로 전환 하는 시간)
    // 추적 상태가 5초간 지속 될 경우, 다음 공격을 시도한다.
    public float AttackRange; // 공격범위 (임시)

    // 보스 체력 --------------------------------------------------------------------

    public float BossPase1HPBody;
    public float BossPase2HPBody;
    public float BossPase1OutTime;
    public float BossPase2OutTime;

    // 핵 체력
    public float BossPase1HPHack;
    public float BossPase2HpHack;
    public HackState m_Hack; // 핵

    public bool isStun = false;
    float bulletPower;
    public bool pase2_on = false; // 2페이즈가 아니다 (핵을 한번이라도 제거하지 못했다)

    public float nowHP;

    //--------------------------------------------------------------------

    Transform m_transform; // 자신의 위치
    Transform p_transform; // 타겟 (플레이어) 위치

    NavMeshAgent nv; // 네비게이션인가 뭔가 그거 막 방해물 자동으로 피하고 그런거 있음

     public Coroutine stunTimer;

    void Start ()
    {
        isDead = false;
        m_transform = gameObject.GetComponent<Transform>();

        // 플레이어 태그를 가진 물체를 찾아서 위치를 가져옴
        p_transform = GameObject.FindWithTag("Player").GetComponent<Transform>();

        nv = gameObject.GetComponent<NavMeshAgent>();
        nv.speed = TrackingSpeed;
        nv.acceleration = nv.speed;

        m_Hack.gameObject.SetActive(false);
        nowHP = BossPase1HPBody;

        StartCoroutine(CheckState());
        StartCoroutine(CheckState_Action());
    }

    IEnumerator CheckState()
    {
        while (!isDead)
        {
            yield return new WaitForSeconds(0.2f);

            float distance = Vector3.Distance(m_transform.position, p_transform.position);

            // 핵 상태 받아오기
            BossPase1HPHack = m_Hack.BossPase1HPHack;
            BossPase2HpHack = m_Hack.BossPase2HpHack;

            // HP 상태
            if (nowHP <= 0 && !isStun)
            { // 보스 몸체의 HP가 0인데 스턴이 아니다.
                isStun = true;
                stunTimer = StartCoroutine(StunTimer()); // 타이머 시작
            }
           
            // 추적 공격
            if (distance <= ViewFieldRange && distance > AttackRange && !isStun) // 거리가 추적범위와 적거나 같다
            {
                curState = Boss_CurrentState.Chase;
            }
            else if (distance <= AttackRange && !isStun) // 거리가 추적범위와 적거나 같다
            {
                curState = Boss_CurrentState.Attack;
            }
            else if (!isStun)
            {
                StopCoroutine(stunTimer); // 스턴이 아니면 타이머 그만
            }
            else if (isStun)
            {
                curState = Boss_CurrentState.Stun;
            }
            else
            {
                curState = Boss_CurrentState.Idle;
            }
        }

        if (isDead)
        {
            curState = Boss_CurrentState.Death;
        }
    }

    IEnumerator CheckState_Action()
    {
        while (!isDead)
        {
            switch (curState)
            {
                case Boss_CurrentState.Idle:
                    nv.isStopped = true; // 정지상태다.
                    break;
                case Boss_CurrentState.Attack:
                    nv.isStopped = true; // 정지상태다.
                    break;
                case Boss_CurrentState.Chase:
                    nv.destination = p_transform.position;
                    nv.isStopped = false; // 정지상태가 아니다
                    break;
                case Boss_CurrentState.Stun: // 스턴상태면
                    nv.isStopped = true; // 정지상태다.
                    break;
                case Boss_CurrentState.Death: // 죽음상태면
                    nv.isStopped = true; // 정지상태다.
                    break;
            }

            yield return null;
        }
    }

    IEnumerator StunTimer()
    {
        m_Hack.gameObject.SetActive(true);
        float time;
        if (!pase2_on)
        {
            time = BossPase1OutTime;
        }
        else
        {
            time = BossPase2OutTime;
        }
        while (isStun)
        {
            yield return new WaitForSeconds(time);
            isStun = false;
            if (!pase2_on)
            {
                if (BossPase1HPHack > 0)
                {
                    nowHP = BossPase1HPBody;
                }
            }
            else
            {
                if (BossPase2HpHack > 0)
                {
                    nowHP = BossPase2HPBody;
                }
            }
            m_Hack.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            bulletPower = col.gameObject.GetComponent<Bullet>().power;
            col.gameObject.GetComponent<Bullet>().DisplayDamage(this.gameObject);
            Destroy(col.gameObject);

            nowHP -= bulletPower;
                if (nowHP <= 0)
                {
                    nowHP = 0;
                }
        }
    }
}
